package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class IntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_void() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/no-return-value"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
    @Test
    void should_return_204_when_set() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/no-return-value-with-annotation"))
                .andExpect(MockMvcResultMatchers.status().is(204));
    }

    @Test
    void should_return_200_text_plain_when_set() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/messages/message"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.CONTENT_TYPE, "text/plain;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.content().string("message"));
    }

    @Test
    void should_return_200_json_when_set() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-objects/message"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.value").value("message"));
    }

    @Test
    void should_return_202_json_when_set() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-objects-with-annotation/message"))
                .andExpect(MockMvcResultMatchers.status().is(202))
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.jsonPath("$.value").value("message"));
    }

    @Test
    void should_return_response_json_when_set() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-entities/message"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().string("{\"value\":\"{message}\"}"))
                .andExpect(MockMvcResultMatchers.header().string("X-Auth", "me"));
    }

    @Test
    void should_return_202_json_when_throw() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/exceptions/runtime-exception"))
                .andExpect(MockMvcResultMatchers.status().is(500));
    }
}
