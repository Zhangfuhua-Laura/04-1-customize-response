package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class webClientIntegrationTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_return_202_json_when_throw_use_web_client() throws Exception {
        ResponseEntity<Void> forEntity = testRestTemplate.getForEntity("/api/exceptions/runtime-exception", Void.class);
        assertEquals(500, forEntity.getStatusCodeValue());
    }
}
