package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @GetMapping("/api/no-return-value")
    void getResponse(){

    }

    @GetMapping("/api/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void getResponse204(){

    }

    @GetMapping("/api/messages/{message}")
    String getResponseMessage(@PathVariable String message){
        return message;
    }

    @GetMapping("/api/message-objects/{message}")
    Message getResponseObject(@PathVariable String message){
        Message mess = new Message(message);
        return mess;
    }

    @GetMapping("/api/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    Message getResponseObjectWithAnnotation(@PathVariable String message){
        return new Message(message);
    }

    @GetMapping("/api/exceptions/runtime-exception")
    public void getResponseWithException(){
        throw new RuntimeException();
    }

    @GetMapping("/api/message-entities/{message}")
    public ResponseEntity<String> getHeader(@PathVariable String message){
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).header("X-Auth", "me").body("{\"value\":\"{message}\"}");
    }
}
